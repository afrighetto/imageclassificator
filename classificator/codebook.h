#ifndef CODEBOOK_H
#define CODEBOOK_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/features2d.hpp>
#include <omp.h>

#include "utils.h"

class CodebookCreation {
	CodebookCreation() { };
	template<typename T>
	void detectAndCluster(features_t method, Feature2D* detector, Feature2D* extractor, vector <pair<int,string>> &fileList, string inputFolder, string outputFile, int minHessian, int clusterSize);
public:
	CodebookCreation(features_t method, string inputFolder, string outputFile, int minHessian, int clusterSize);
	~CodebookCreation() { };
};

#endif // CODEBOOK_H