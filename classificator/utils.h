#ifndef UTILS_H
#define UTILS_H

#include <cassert>
#include <cstring>
#include <ctime>
#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <typeinfo>
#include <unistd.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/flann.hpp>

#include "log.h"

using namespace std;
using namespace cv;

typedef enum { F_SURF, F_SIFT, F_KAZE, F_ORB } features_t;
typedef enum { H_FISHER, H_BOW } histograms_t;

int num_dirs(const char* path, vector<string>& dir_list);

int num_images(const char* path);
int num_images(const char* path, vector<string>& file_list);

float strtofloat(const string& what);

extern LogStream logStream;
extern string homePath;

const char* featureToString(features_t feature);
const char* histogramToString(histograms_t histogram);
string toGlobalPath(string localPath);
string toLocalPath(string globalPath);

time_t fileLastModification(const char* filename);

void kMajority(InputArray data, OutputArray centers, int maxIterations);
int hamming(uchar a, uchar b);

#endif // UTILS_H
