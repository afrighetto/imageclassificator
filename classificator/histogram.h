#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <iomanip>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#ifdef __cplusplus
extern "C" {
#endif

#include <vl/generic.h>
#include <vl/gmm.h>
#include <vl/fisher.h>

#ifdef __cplusplus
}
#endif

#include "utils.h"

using namespace cv;

class HistogramCreation {
	string datasetFolder;
	string method;
	string histogramMethod;
	int minHessian;
	int clusterSize;
	int descriptorSize;
	int datasetSize;
	vector<string> categories;
	VlGMM* gmm;
	
	HistogramCreation() { };

public:
	HistogramCreation(histograms_t histogramMethod, string inputTestingDataset, string codebookFile);
	~HistogramCreation() { };
	
private:
	bool readCodebookMetadata(fstream &fin);
	template <typename T> void readCodebook(fstream &fin, cv::Mat &codebook);
	void setupGMM(const cv::Mat &codebook);

	vector<float> createHistogram(histograms_t histogramMethod, const cv::Mat &codebook, const cv::Mat &descriptors);
	vector<float> createBOWHistogram(const cv::Mat &codebook, const cv::Mat &descriptors);
	vector<float> createBOWHistogramORB(const cv::Mat &codebook, const cv::Mat &descriptors);
	vector<float> createFisherHistogram(const cv::Mat &descriptors, int dimension);

	float accuracy(const vector<vector<int>> &confusion);
	float precision(const vector<int> col, int index);
	float recall(const vector<int> row, int index);
	void createConfusionMatrixImg(const vector<vector<int>> &confusion, const char* img_path);
};

#endif // HISTOGRAM_H
