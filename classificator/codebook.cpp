#include "codebook.h"

CodebookCreation::CodebookCreation(features_t method, string inputFolder, string outputFile, int minHessian, int clusterSize)
{
    vector<string> dir_list;

    logStream << "\nCODEBOOK -- using method: " << featureToString(method);
    if(method == F_SURF)
        logStream << ", minHessian: " << minHessian;
    logStream << ", clusterSize: " << clusterSize << "\n";
	
	if(num_dirs(inputFolder.c_str(), dir_list) < 0)
    {
        cerr << "[-] Incorrect input folder, aborting...\n";
        return;
    }
	
	// Create file list using splitPercent files of each directory
    vector<pair<int,string>> fileList;
    for(unsigned int dirIx = 0; dirIx < dir_list.size(); dirIx++)
    {
        char path[256];
        snprintf(path, sizeof(path), "%s/%s", inputFolder.c_str(), dir_list[dirIx].c_str());
        vector<string> pathFileList;

        int dirFileCount = num_images(path, pathFileList);

        for(int fileIx = 0; fileIx < dirFileCount; fileIx++)
        {
            snprintf(path, sizeof(path), "%s/%s/%s", inputFolder.c_str(), dir_list[dirIx].c_str(), pathFileList[fileIx].c_str());
            fileList.push_back(pair<int,string>(dirIx, path));
        }
    }
	
    Feature2D *detector = NULL;
    Feature2D *extractor = NULL;
	
    if(method == F_SIFT)
    {
        detector = new SiftFeatureDetector();
        extractor = new SiftDescriptorExtractor();
		detectAndCluster<float>(method, detector, extractor, fileList, inputFolder, outputFile, minHessian, clusterSize);
    }
    else if(method == F_SURF)
    {
        detector = new SurfFeatureDetector(minHessian);
        extractor = new SurfDescriptorExtractor();
		detectAndCluster<float>(method, detector, extractor, fileList, inputFolder, outputFile, minHessian, clusterSize);
    }
    else if(method == F_KAZE)
    {
        detector = new KAZE();
        extractor = new KAZE();
		detectAndCluster<float>(method, detector, extractor, fileList, inputFolder, outputFile, minHessian, clusterSize);
    }
	else if(method == F_ORB) 
	{
		detector = new ORB();
		extractor = new ORB();
		detectAndCluster<uchar>(method, detector, extractor, fileList, inputFolder, outputFile, minHessian, clusterSize);
	}    
}

template<typename T> 
void CodebookCreation::detectAndCluster(features_t method, Feature2D* detector, Feature2D* extractor, vector <pair<int,string>> &fileList, string inputFolder, string outputFile, int minHessian, int clusterSize) 
{
	struct timespec start, img_read, clustering;
	Mat codebook;
	
	if(detector == NULL || extractor == NULL)
    {
        cerr << "[-] Initialization error, aborting...\n";
        return;
    }
	
	clock_gettime(CLOCK_REALTIME, &start);

    int totalKeypoints = 0;
    int totalFilesCount = 0;

    vector<vector<T>> featuresMat;

	#pragma omp parallel for shared(detector,extractor,totalFilesCount,featuresMat)
    for( unsigned int fileIx = 0; fileIx<fileList.size(); fileIx++ )
    {

        Mat img = imread(fileList[fileIx].second.c_str(), 1);

        cout << "\r[+] Processing image " << toGlobalPath(fileList[fileIx].second) << endl; // << flush; // output current filename (cout only, not to logFile)
		fflush(stdout);
		
        vector<KeyPoint> keypoints;
        detector->detect(img, keypoints);

        Mat descriptors;
        extractor->compute(img, keypoints, descriptors);

        // Cut points which normalization goes above 0.2 to reduce error given by luminosity -- SIFT only
        if(method == F_SIFT)
        {
            for(int i=0; i<descriptors.rows; i++)
            {
                long double sum = 0;

                // l2 norm
                for(int j = 0; j<descriptors.cols; j++)
                    sum += (descriptors.at<float>(i,j)*descriptors.at<float>(i,j));

                sum = sqrt(sum);

                for(int j=0; j<descriptors.cols; j++)
                    descriptors.at<float>(i,j) /= sum;

                // cut values above 0.2
                for(int j = 0; j<descriptors.cols; j++)
                    descriptors.at<float>(i,j) = min( 0.2f, descriptors.at<float>(i,j) );

                // l2 norm
                sum = 0;

                for(int j=0; j<descriptors.cols; j++)
                    sum += (descriptors.at<float>(i,j)*descriptors.at<float>(i,j));

                sum = sqrt(sum);

                for(int j=0; j<descriptors.cols; j++)
                    descriptors.at<float>(i,j) /= sum;
            }
        }

        #pragma omp critical
        {
            for( int i=0; i<descriptors.rows; i++ )
            {
                vector<T> tmp;
                for(int j = 0; j<descriptors.cols; j++)
                    tmp.push_back(descriptors.at<T>(i,j));

                featuresMat.push_back(tmp);
            }

            totalKeypoints += keypoints.size();
            totalFilesCount++;
        }
    }

    // done reading images
    cout << "[+] Clustering...\n";
    clock_gettime(CLOCK_REALTIME, &img_read);

	logStream << "\n";
    logStream << "Extracted " << totalKeypoints << " keypoints from " << totalFilesCount << " images\n";
    if(totalKeypoints < clusterSize)
    {
        clusterSize = totalKeypoints;
        logStream << "Warning: totalKeypoints is less than desired clusterSize! New clusterSize is " << totalKeypoints << "\n";
    }

    int descriptorLen = method == F_SIFT ? 128 : method == F_ORB ? 32 : 64;

    Mat data(featuresMat.size(), descriptorLen, method == F_ORB ? CV_8UC1 : CV_32F);
    for(unsigned int s = 0; s<featuresMat.size(); s++)
		for(int i=0; i<descriptorLen; i++)
			data.at<T>(s, i) = featuresMat[s][i];

    // do cluster
    Mat clusterLabels;
    Mat centers(clusterSize, descriptorLen, method == F_ORB ? CV_8UC1 : CV_32F);
	if (method == F_ORB) {
		kMajority(data,centers,35);
	} else {
		try {
			kmeans(data, clusterSize, clusterLabels, TermCriteria( CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.0001), 20, KMEANS_PP_CENTERS, centers);
		} catch(const std::exception& e) {
			cerr << "[e] Number of clusters cannot be equal to 0, exception raised: " << e.what();
			return;
		}
	}
	
    codebook = centers;

    clock_gettime(CLOCK_REALTIME, &clustering);

    // save codebook file
    ofstream fout;

    fout.open(outputFile.c_str(), ios::out);
    if(!fout.is_open())
    {
        cerr << "[-] Error writing codebook file, aborting...\n";
        return;
    }

    // convert inputFolder from localPath to globalPath (replace homePath with tilde)
    inputFolder = toGlobalPath(inputFolder);

    // First row: metadata
    fout << "Feature-detection method: " << featureToString(method);
    if(method == F_SURF)
        fout << ", minHessian: " << minHessian;
    fout << ", clusterSize: " << codebook.rows << ", descriptorsSize: " << codebook.cols;
    fout << ", dataset: '" << inputFolder << "'" << endl;

    for(int i = 0; i<codebook.rows; i++)
    {
        for(int j = 0; j<codebook.cols; j++)
		{
			if (method == F_ORB) 
				fout << (int)codebook.at<T>(i,j) << "|";
			else
				fout << codebook.at<T>(i,j) << "|";
		}
        fout << endl;
    }
    fout.close();

    // print times
    int imgReadingMins = (img_read.tv_sec - start.tv_sec) / 60;
    int imgReadingSecs = (img_read.tv_sec - start.tv_sec) % 60;
    int clusteringMins = (clustering.tv_sec - img_read.tv_sec) / 60;
    int clusteringSecs = (clustering.tv_sec - img_read.tv_sec) % 60;

    logStream << "Image Reading:   " << imgReadingMins << "m, " << imgReadingSecs << "s.\n";
    logStream << "Clustering:      " << clusteringMins << "m, " << clusteringSecs << "s.\n";
	logStream.flush();
	cout << "[+] Codebook created at " << outputFile << " successfully.\n";
	cout << "[+] Open logile.log for further info.\n";
}