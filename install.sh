#!/bin/bash

abort() {
    echo "[-] ${1:-Something went wrong}." >&2
    exit 1
}

if [[ "$OSTYPE" != "linux-gnu" ]]; then
    echo "[i] Not been tested on your OS..."
fi

if [[ "$EUID" != 0 ]]; then
    echo "[-] Run as root." >&2
    exit 1
fi

#Install updated packages
apt-get -y update && apt-get -y upgrade

#Prepare environment to build OpenCV
apt-get -y install build-essential cmake pkg-config

#Install Image/Video I/O libraries
apt-get -y install libjpeg-dev libjpeg62-dev libtiff4-dev libjasper-dev libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libtbb-dev

#Install OpenCL
apt-get -y install ocl-icd-opencl-dev

#Install Python dev environment
apt-get -y install python-dev python-numpy

#Install Qt dev libraries
apt-get -y install libqt4-dev

#Install OpenCV
cd ~ && wget https://github.com/BloodAxe/opencv/archive/kaze.zip || abort "Could not install OpenCV, please retry"
unzip kaze.zip
rm kaze.zip
cd opencv-kaze
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_GTK=ON -D WITH_OPENGL=ON ..
make && make install

#Install VLFeat and its library
cd ~ && wget http://www.vlfeat.org/download/vlfeat-0.9.20-bin.tar.gz || abort "Could not install VLFeat, please retry"
tar -xf vlfeat-0.9.20-bin.tar.gz
rm vlfeat-0.9.20-bin.tar.gz
cd vlfeat-0.9.20
mkdir /usr/local/include/vl
cp vl/*.h /usr/local/include/vl/
mkdir /usr/local/lib/vl
if [[ "$(getconf LONG_BIT)" == 64 ]]; then
    cp bin/glnxa64/libvl.so /usr/local/lib/
else
    cp bin/glnx86/libvl.so /usr/local/lib/
fi
echo "/usr/local/lib/vl" > /etc/ld.so.conf.d/vl.conf
ldconfig

echo "[+] All done."
exit 0
