#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <math.h>
#include <pwd.h>
#include <thread>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/nonfree/nonfree.hpp>

#include "classificator/utils.h"
#include "classificator/codebook.h"
#include "classificator/histogram.h"

#define CODEBOOK_FLAG  1 << 0
#define TRAINSVM_FLAG  1 << 1

typedef struct {
	union {
		features_t featureMethod;
		histograms_t histogramsMethod;
	};
	string inputTrainingFolder;
	string inputTestFolder;
	string codebookFile;
	int minHessian;
	int clusterSize;
} config_t;

void parseConfigFile(void* config, const char* path, int mode) {
	assert(path);
	ifstream inputFile(path);
	if (!inputFile.is_open()) {
		cerr << "[-] Could not open " << path << endl;
		exit(1);
	}
	config_t* configFile = static_cast<config_t*>(config);
	string line;
	map<string, string> dict;
	while (inputFile && getline(inputFile, line)) {
		if (line[0] == '#' || line.empty())
			continue;
		dict.insert(std::make_pair(line.substr(0, line.find(' ')), line.substr(line.rfind(' ') + 1)));		
	}
	
	for (auto& str : dict) {
		if (str.first == "FEATURE_DETECTION_METHOD" && mode & CODEBOOK_FLAG) {
			string method = str.second;
			if (method == "SURF") {
				configFile->featureMethod = F_SURF;
				configFile->minHessian = stoi(dict.find("MIN_HESSIAN")->second);
			} else {
				if (method == "SIFT")
					configFile->featureMethod = F_SIFT;
				else if (method == "KAZE")
					configFile->featureMethod = F_KAZE;
				else
					configFile->featureMethod = F_ORB;
				configFile->minHessian = 0;
			}
		}
		else if (str.first == "HISTOGRAMS_CREATE_METHOD" && mode & TRAINSVM_FLAG) {
			string method = str.second;
			if (method == "FISHER")
				configFile->histogramsMethod = H_FISHER;
			else
				configFile->histogramsMethod = H_BOW;
		}
		else if (str.first == "CLUSTER_SIZE") {
			configFile->clusterSize = stoi(str.second);
		}
		else if (!str.first.compare(0, 7, "DATASET")) {
			if (str.first.find("TRAINING") != string::npos) {
				configFile->inputTrainingFolder = str.second;
			} else {
				configFile->inputTestFolder = str.second;			
			}
		}
		else if (str.first == "CODEBOOK_FILE_OUTPUT") {
			if (str.second.substr(str.second.rfind('.') + 1) != "cbk")  //Even if not necessary
				str.second.append(".cbk");
			configFile->codebookFile = str.second;
		}
	}
	inputFile.close();
}

void usage(const char* path) { 
	cout << "Usage: " << path << " -i [file...] [ -c | -t ]"; 
	exit(1); 
}

int main(int argc, const char *argv[]) {
	int flag = 0;
	const char* confPath;
	if (argc != 4) {
		cerr << "[-] Invalid arguments. ";
		usage(argv[0]);
	}
	
	for (int i = 1; i < argc && argv[i][0] == '-'; i++) {
		switch (argv[i][1]) {
			case 'i':
				confPath = &argv[++i][0];
				break;
			case 'c':
				flag |= CODEBOOK_FLAG;
				break;
			case 't':
				flag |= TRAINSVM_FLAG;
				break;
			default:
				usage(argv[0]);
		}
	}
	
	const char* extension = strrchr(confPath, '.');
	if (!extension || strcmp(&extension[0], ".conf")) {
		cerr << "[-] Invalid configuration file.\n";
		return -1;
	}
	
	struct passwd *pw = getpwuid(getuid());
    homePath = pw->pw_dir;
	
	logStream.Init("logfile.log");
	config_t configFile;
	cout << "[+] Parsing " << confPath << endl;
	parseConfigFile(&configFile, confPath, flag);
	
#if __cplusplus > 199711L
	thread threadObj(
		[&]() mutable {
			if (flag & CODEBOOK_FLAG)
				unique_ptr<CodebookCreation> codebook(
					new CodebookCreation(configFile.featureMethod, configFile.inputTrainingFolder, configFile.codebookFile, configFile.minHessian, configFile.clusterSize)
				);
			else
				unique_ptr<HistogramCreation> train(
					new HistogramCreation(configFile.histogramsMethod, configFile.inputTestFolder, configFile.codebookFile)
				);
		}
	);
	
	threadObj.join();
#else
	if (flag & CODEBOOK_FLAG) {
		CodebookCreation* codebook = new CodebookCreation(configFile.featureMethod, configFile.inputTrainingFolder, configFile.codebookFile, configFile.minHessian, configFile.clusterSize);
		delete codebook;
	} else {
		HistogramCreation* train = new HistogramCreation(configFile.histogramsMethod, configFile.inputTestFolder, configFile.codebookFile);
		delete train;
	}
#endif

}
