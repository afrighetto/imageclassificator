all:
	g++ -g -Wall -O2 -std=c++11 main.cpp classificator/*.cpp -o imageclassificator -fopenmp -L/usr/local/lib/vl `pkg-config opencv --libs` -lvl

clean:
	rm imageclassificator
