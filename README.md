
### ImageClassificator
----

A simple tool to test the performance of image classificators based on local features extracted using Scale-Invariant Feature Transform (SIFT), Speeded Up Robust Features (SURF), KAZE or ORB and encoded using Bag of Words or Fisher Vector algorithms.
